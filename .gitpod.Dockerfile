FROM python:3.11-bullseye
RUN apt update && \
    apt install skopeo --yes && \
    pip install -U hoppr==1.7.0